/// <reference path="../../DefinitelyTyped/knockout/knockout.d.ts" />

import MovieEntityModule = require("../Entities/Movie");
import MovieServiceModule = require("../Services/MovieService");

export class MovieListViewModel  {

    public movieList: KnockoutObservableArray<MovieEntityModule.Movie>;
    
    constructor()
    {
        var movieListService: MovieServiceModule.MovieService = new MovieServiceModule.MovieService();
        var movies = movieListService.GetAllMovies();
        this.movieList = ko.observableArray<MovieEntityModule.Movie>(movies);
    }
}
