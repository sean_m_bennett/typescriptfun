/// <reference path="../../DefinitelyTyped/knockout/knockout.d.ts" />
define(["require", "exports", "../Services/MovieService"], function(require, exports, __MovieServiceModule__) {
    
    var MovieServiceModule = __MovieServiceModule__;

    var MovieListViewModel = (function () {
        function MovieListViewModel() {
            var movieListService = new MovieServiceModule.MovieService();
            var movies = movieListService.GetAllMovies();
            this.movieList = ko.observableArray(movies);
        }
        return MovieListViewModel;
    })();
    exports.MovieListViewModel = MovieListViewModel;
});
