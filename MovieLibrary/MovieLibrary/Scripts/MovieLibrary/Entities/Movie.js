define(["require", "exports"], function(require, exports) {
    var Movie = (function () {
        function Movie(Title, ReleaseDate) {
            this.Title = Title;
            this.ReleaseDate = ReleaseDate;
        }
        Movie.prototype.GetMovieTitle = function () {
            return this.Title + ' ' + this.ReleaseDate.getFullYear();
        };
        return Movie;
    })();
    exports.Movie = Movie;
});
