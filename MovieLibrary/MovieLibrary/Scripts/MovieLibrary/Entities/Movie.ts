export interface IMovie {
    
}

export class Movie implements IMovie {
    constructor(public Title: string, public ReleaseDate: Date)
    {
    }

    public GetMovieTitle(): string
    {
        return this.Title + ' ' + this.ReleaseDate.getFullYear();
    }
}
