/// <reference path="../Entities/Movie.ts" />

import MovieEntityModule = require('../Entities/Movie');

export interface IMovieService {

}

export class MovieService implements IMovieService {
    constructor()
    {
    }

    public GetAllMovies(): Array
    {
        var list = new Array();

        list.push(new MovieEntityModule.Movie("title1", new Date()));
        list.push(new MovieEntityModule.Movie("title2", new Date()));
        list.push(new MovieEntityModule.Movie("title3", new Date()));
        list.push(new MovieEntityModule.Movie("title4", new Date()));
        list.push(new MovieEntityModule.Movie("title5", new Date()));

        return list;
    }
}