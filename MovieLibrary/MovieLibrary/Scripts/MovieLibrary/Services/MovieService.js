/// <reference path="../Entities/Movie.ts" />
define(["require", "exports", '../Entities/Movie'], function(require, exports, __MovieEntityModule__) {
    var MovieEntityModule = __MovieEntityModule__;

    var MovieService = (function () {
        function MovieService() {
        }
        MovieService.prototype.GetAllMovies = function () {
            var list = new Array();

            list.push(new MovieEntityModule.Movie("title1", new Date()));
            list.push(new MovieEntityModule.Movie("title2", new Date()));
            list.push(new MovieEntityModule.Movie("title3", new Date()));
            list.push(new MovieEntityModule.Movie("title4", new Date()));
            list.push(new MovieEntityModule.Movie("title5", new Date()));

            return list;
        };
        return MovieService;
    })();
    exports.MovieService = MovieService;
});
