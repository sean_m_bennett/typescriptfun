﻿require.config({
    baseUrl: '/Scripts/MovieLibrary',
    paths: {
        jquery: '../jquery-1.8.2',
    },
    shim: {
        'jquery': {
            exports: '$'
        }
        
    }

});

require(['ViewModels/MovieListViewModel', 'jquery'], function (module) {
    var viewModel = new module.MovieListViewModel();
    ko.applyBindings(viewModel, $("#MovieList")[0]);
});